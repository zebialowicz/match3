﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CellScalingDownBehaviour : StateMachineBehaviour
{  
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {        
        animator.transform.gameObject.GetComponent<SingleCellView>().ScaledDownListener();
    }
}
