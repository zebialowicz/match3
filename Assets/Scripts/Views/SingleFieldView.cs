﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class SingleFieldView : MonoBehaviour, IPointerClickHandler
{
    public event Action OnCellSelected;

    public Image fieldSprite;


    /// <summary>
    /// Call action if user click on the field
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData)
    {
        OnCellSelected?.Invoke();
    }

    /// <summary>
    /// Set new position on the view
    /// </summary>
    /// <param name="targetPosition"></param>
    public void SetPosition(Vector2 targetPosition)
    {
        transform.localPosition = targetPosition;
    }
    

    /// <summary>
    /// Highlight view (as available to click)
    /// </summary>
    public void HighlightField()
    {
        fieldSprite.color = Color.green;
    }

    /// <summary>
    /// Set view as not available for click
    /// </summary>
    public void DisableField()
    {
        fieldSprite.color = Color.red;
    }
}
