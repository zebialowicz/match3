﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SingleCellView : MonoBehaviour
{
    [SerializeField] private Image spriteRender;

    public event Action<SingleCellView> OnAnimationComplete;

    public Animator cellAnimator;

    /// <summary>
    /// Change sprite color to match color in model of the cell
    /// </summary>
    /// <param name="targetColor"></param>
    public void ChangeSpriteColor(Color32 targetColor)
    {
        spriteRender.color = targetColor;
    }

    /// <summary>
    /// Scaling up sprite from view
    /// </summary>
    public void ScaleUp()
    {
        cellAnimator.SetTrigger("ScaleUp");
    }

    /// <summary>
    /// Scaling down sprite from view
    /// </summary>
    public void ScaleDown()
    {        
        cellAnimator.SetTrigger("ScaleDown");
    }

    /// <summary>
    /// Listener for end of animation scaling down
    /// </summary>
    public void ScaledDownListener()
    {
        Debug.Log("Scaled Down Finished");
        OnAnimationComplete?.Invoke(this);
    }
}
