﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBoardModel
{
    public GameObject[,] boardFields { get; private set; }

    public int columns { get; private set; }
    public int rows { get; private set; }

    /// <summary>
    /// Create new grid board with given data
    /// </summary>
    /// <param name="_width">columns</param>
    /// <param name="_height">rows</param>
    /// <param name="fields">fields to assign</param>
    /// <param name="fieldsProcessor">reference to fields Processor</param>
    public GridBoardModel(int _width, int _height, List<GameObject> fields, FieldsProcessor fieldsProcessor)
    {
        columns = _width;
        rows = _height;

        boardFields = new GameObject[columns, rows];

        int fieldIndex = 0;

        for(int row = 0; row < columns; row++)
        {
            for(int column = 0; column < rows; column++)
            {
                boardFields[row, column] = fields[fieldIndex];

                var fieldController = fieldsProcessor.GetSingleFieldController(fields[fieldIndex].GetComponent<SingleFieldView>());
                fieldController.AssingIndexPositionInArray(new Vector2(row, column));

                fieldIndex++;
            } 
        }
    }
}
