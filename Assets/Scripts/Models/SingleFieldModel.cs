﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleFieldModel
{
    /// <summary>
    /// Unique cell index
    /// </summary>
    public int cellIndex { get; set; }

    /// <summary>
    /// is field available to select
    /// </summary>
    public bool isAvailableToSelect = false;

    /// <summary>
    /// position on the grid model
    /// </summary>
    public Vector2 positionOnGrid { get; set; }


    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="index">index of the field</param>
    public SingleFieldModel(int index)
    {
        cellIndex = index;
    }
}
