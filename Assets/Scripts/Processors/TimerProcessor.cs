﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TimerProcessor : MonoBehaviour
{
    /// <summary>
    /// Called when timer reach the given value
    /// </summary>
    public event Action OnTimeUp;

    [SerializeField] private float timer = 0f;
    [SerializeField] private bool countTime = false;

    [SerializeField] private float targetTime = 0f;

    private void Update()
    {
        if (countTime)
        {
            timer += Time.deltaTime;

            if(timer >= targetTime)
            {
                StopTimer();
            }
        }    
    }

    /// <summary>
    /// Start the timer
    /// Set the target time
    /// </summary>
    /// <param name="_targetTime"></param>
    public void StartTimer(float _targetTime)
    {
        targetTime = _targetTime;
        countTime = true;
    }

    /// <summary>
    /// Stop the timer and reset values
    /// </summary>
    void StopTimer()
    {
        countTime = false;
        timer = 0f;
        OnTimeUp?.Invoke();
    }
}
