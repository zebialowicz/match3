﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldsProcessor : MonoBehaviour
{
    [SerializeField]
    private GridDesigner _gridDesigner;

    public List<SingleFieldController> availableFields = new List<SingleFieldController>();

    private void Start()
    {
        CreateFields();

        Debug.Log("Cells count: " + availableFields.Count);
    }

    /// <summary>
    /// Create fields controllers based on field views in grid designer
    /// </summary>
    void CreateFields()
    {
        int fieldIndex = 0;

        foreach(var field in _gridDesigner.GetGridFields())
        {
            
            SingleFieldModel model = new SingleFieldModel(fieldIndex);

            SingleFieldController singleField = new SingleFieldController(
                model, //field model
                field.GetComponent<SingleFieldView>() //field view
                );

            singleField.MarkAsAvailableToSelected(true);

            availableFields.Add(singleField);

            fieldIndex++;
        }
    }

    /// <summary>
    /// Get All Fields Controllers created
    /// </summary>
    /// <returns></returns>
    public List<SingleFieldController> GetFieldControllers()
    {
        return availableFields;
    }

    /// <summary>
    /// Get Controller assigned to given view
    /// </summary>
    /// <param name="fieldView"></param>
    /// <returns></returns>
    public SingleFieldController GetSingleFieldController(SingleFieldView fieldView)
    {
        return availableFields.Find(x => x.view == fieldView);
    }
}
