﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CellsProcessor : MonoBehaviour
{
    [SerializeField] private GridDesigner _gridDesigner;

    public GameObject singleCellPrefab;

    private List<GameObject> cellsObjects = new List<GameObject>();
    private List<SingleCellController> cellsControllers = new List<SingleCellController>();

    private List<Color32> colors = new List<Color32>();

    private void Start()
    {
        if(_gridDesigner == null)
        {
            Debug.LogError("Grid designer is not assigned !");
            return;
        }

        SpawnCells();
    }

    /// <summary>
    /// Spawn all cells on the board
    /// </summary>
    void SpawnCells()
    {
        //Get colors set from grid designer
        colors = _gridDesigner.AvailableColors.ToList();

        //Spawn new cell for each of the field
        foreach(var field in _gridDesigner.GetGridFields())
        {
            var singleCell = Instantiate(singleCellPrefab, field.transform);
            cellsObjects.Add(singleCell);

            SingleCellModel cellModel = new SingleCellModel();

            SingleCellController cellController = new SingleCellController(
                cellModel, 
                singleCell.GetComponent<SingleCellView>()
                );

            //Set init data
            Random.InitState(_gridDesigner.seed);
            cellController.SetCellData(colors[Random.Range(0, colors.Count)]);
            cellController.TriggerScaleUpAnim();

            //Add controller to list of created controllers
            cellsControllers.Add(cellController);
        }
    }

    /// <summary>
    /// Get all created cell controllers
    /// </summary>
    /// <returns>List of cell controllers</returns>
    public List<SingleCellController> GetCellsControllers()
    {
        return cellsControllers;
    }

    /// <summary>
    /// Return List of available colors 
    /// </summary>
    /// <returns>List colors</returns>
    public List<Color32> GetAvailableColors()
    {
        return colors;
    }


    /// <summary>
    /// Generate new data for specific cell controller
    /// </summary>
    /// <param name="cellController"></param>
    public void GenerateNewCell(SingleCellController cellController)
    {
        Random.State randomState = Random.state;
        cellController.SetCellData(colors[Random.Range(0, colors.Count)]);      
    }

    /// <summary>
    /// Find and return a single cell controller connected to given cell view
    /// </summary>
    /// <param name="cellView"></param>
    /// <returns>Single Cell Controller</returns>
    public SingleCellController GetSingleController(SingleCellView cellView)
    {
        return cellsControllers.Find(x => x.view == cellView);
    }

}
