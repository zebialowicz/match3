﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class GridProcessor : MonoBehaviour
{
    #region Processors
    [Header("Processors")]
    public FieldsProcessor fieldsProcessor;
    public CellsProcessor cellsProcessor;
    public TimerProcessor timerMatchProcessor;
    #endregion

    [SerializeField]
    private GameObject[] SelectedGridFields = new GameObject[2];

    private List<GameObject> nearestObjects = new List<GameObject>();

    private List<SingleFieldController> fieldsControllers = new List<SingleFieldController>();

    private List<GameObject> matchedFields = new List<GameObject>();

    private GridBoardController gridBoardController;
    public GameObject GridBoardPrefab;

    [SerializeField]
    private GridDesigner _gridDesigner;

    private bool isMatchingProcessing = false;

    #region INIT
    private void OnEnable()
    {
        SingleFieldController.OnControllerSelected += SetSelectedField;
        GridBoardController.OnCheckMatches += CheckMatch;
        timerMatchProcessor.OnTimeUp += CheckMatch;
    }

    private void OnDisable()
    {
        SingleFieldController.OnControllerSelected -= SetSelectedField;
        GridBoardController.OnCheckMatches -= CheckMatch;
        timerMatchProcessor.OnTimeUp -= CheckMatch;
    }

    private void Start()
    {
        CreateGridBoardController();
    }
    #endregion

    /// <summary>
    /// Create new Grid Board object and assign data [model, view, controller]
    /// </summary>
    void CreateGridBoardController()
    {
        GridBoardModel model = new GridBoardModel(
            _gridDesigner.gridRows, 
            _gridDesigner.gridColumns,
            _gridDesigner.GetGridFields(),
            fieldsProcessor
            );

        gridBoardController = new GridBoardController(
            model, 
            GridBoardPrefab.GetComponent<GridBoardView>(), 
            cellsProcessor);

        //Randomize data on the grid
        gridBoardController.RandomizeOnInit(_gridDesigner.seed);

    }

    public void SetSelectedField(GameObject targetField)
    {
        if (isMatchingProcessing)
        {
            return;
        }

        fieldsControllers = fieldsProcessor.GetFieldControllers();

        //Reset value if clicked again
        if (targetField == SelectedGridFields[0])
        {
            SelectedGridFields[0] = null;

            Debug.Log("You selected same element");

            SetAllFieldsStatus(true);

            return;
        }

        if(SelectedGridFields[0] == null)
        {
            SelectedGridFields[0] = targetField;

            //Highlight fields we can select now
            HighlightNearest();
        }
        else
        {
            SelectedGridFields[1] = targetField;
            SetNewPosition();
        }
    }

    /// <summary>
    /// Swap position of the 2 fields on the board.
    /// Update their index locally per controller [AssingIndexPositionInArray]
    /// Update grid data [SwitchFieldOnGrid]
    /// </summary>
    void SetNewPosition()
    {
        Vector2 originPosition = SelectedGridFields[0].transform.localPosition;
        Vector2 targetPosition = SelectedGridFields[1].transform.localPosition;
    
        //assign first field controller and swap position on the view
        var firstController = fieldsProcessor.GetSingleFieldController(SelectedGridFields[0].GetComponent<SingleFieldView>());
        firstController.UpdateFieldPosition(targetPosition);

        //assign second field controller and swap position on the view
        var secondController = fieldsProcessor.GetSingleFieldController(SelectedGridFields[1].GetComponent<SingleFieldView>());
        secondController.UpdateFieldPosition(originPosition);

        //swap position of the fields in grid 
        var tmpControllerPosition = firstController.model.positionOnGrid;
        firstController.AssingIndexPositionInArray(secondController.model.positionOnGrid);
        secondController.AssingIndexPositionInArray(tmpControllerPosition);

        gridBoardController.SwitchFieldOnGrid(firstController.model.positionOnGrid, secondController.model.positionOnGrid);

        //Set all fields as available to select
        SetAllFieldsStatus(true);

        //Check if we have matches after swap
        CheckMatch();
    }

    /// <summary>
    /// Ray nearest fields and highlight them if they are available to select
    /// </summary>
    void HighlightNearest()
    {    
        #region Rays
        //top
        RaycastHit2D[] rayHitsTop = Physics2D.RaycastAll(SelectedGridFields[0].transform.position + new Vector3(50, 50, 0), Vector2.up, 100f);

        //down
        RaycastHit2D[] rayHitsDown = Physics2D.RaycastAll(SelectedGridFields[0].transform.position + new Vector3(50, 50, 0), Vector2.down, 100f);

        //right
        RaycastHit2D[] rayHitsRight = Physics2D.RaycastAll(SelectedGridFields[0].transform.position + new Vector3(50, 50, 0), Vector2.right, 100f);

        //left
        RaycastHit2D[] rayHitsLeft = Physics2D.RaycastAll(SelectedGridFields[0].transform.position + new Vector3(50, 50, 0), Vector2.left, 100f);
        #endregion

        List<RaycastHit2D> allDetectedRays = new List<RaycastHit2D>();

        //Add add ranges to detected rays list
        allDetectedRays.AddRange(rayHitsTop.ToList());
        allDetectedRays.AddRange(rayHitsDown.ToList());
        allDetectedRays.AddRange(rayHitsRight.ToList());
        allDetectedRays.AddRange(rayHitsLeft.ToList());

        if (allDetectedRays.Count > 0)
        {
            SetAllFieldsStatus(false);

            foreach (var rayHit in allDetectedRays) 
            {
                if (rayHit.collider != null)
                {

                    //Get specific field controller
                    var targetField = fieldsControllers.Find(x => x.view == rayHit.collider.GetComponent<SingleFieldView>());

                    targetField.MarkAsAvailableToSelected(true);
                }
            }
        }
    }

    /// <summary>
    /// Set all fields on the board as available or not to click
    /// </summary>
    /// <param name="status"></param>
    void SetAllFieldsStatus(bool status)
    {
        foreach (var fieldController in fieldsControllers)
        {
            fieldController.MarkAsAvailableToSelected(status);
        }
    }

    /// <summary>
    /// Update cell with new data if were matched in current move
    /// </summary>
    /// <param name="cellView"></param>
    void UpdateGridCells(SingleCellView cellView)
    {
        var cellController = cellsProcessor.GetSingleController(cellView);

        Debug.Log("Updating grid cells");

        cellsProcessor.GenerateNewCell(cellController);
        cellController.TriggerScaleUpAnim();

        cellView.OnAnimationComplete -= UpdateGridCells;

        //Start checking new matches after [time] if found matches before
        timerMatchProcessor.StartTimer(1.1f);
    }

    /// <summary>
    /// Check if matches exist on the board
    /// </summary>
    void CheckMatch()
    {
        isMatchingProcessing = true;
        Debug.Log("Checking matches");

        //Clear assigned data
        SelectedGridFields[0] = null;
        SelectedGridFields[1] = null;

        List<GameObject> matchedFields = gridBoardController.FindMatches();

        //if matched field < 3 do nothing [we required at least 3 same cells in a row/column]
        if(matchedFields.Count < 3)
        {
            isMatchingProcessing = false;
            return;
        }

        //Set new data for every matched field
        foreach(var field in matchedFields)
        {
            var cell = field.transform.GetChild(0).gameObject;
            var cellView = cell.GetComponent<SingleCellView>();
            var cellController = cellsProcessor.GetSingleController(cellView);

            cellView.OnAnimationComplete += UpdateGridCells;

            cellController.TriggerScaleDownAnim();
        }

        matchedFields.Clear();

        SetAllFieldsStatus(true);
    }
}
