﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class SingleFieldController
{
    public static event Action<GameObject> OnControllerSelected;

    public SingleFieldModel model { get; private set; }
    public SingleFieldView view { get; private set; }

    /// <summary>
    /// Create controller
    /// </summary>
    /// <param name="_model"></param>
    /// <param name="_view"></param>
    public SingleFieldController(SingleFieldModel _model, SingleFieldView _view)
    {
        model = _model;
        view = _view;

        view.OnCellSelected += ControllerSelected;
    }

    /// <summary>
    /// Call action when this controller is selected
    /// </summary>
    public void ControllerSelected()
    {
        Debug.Log("Selected controller with: " + model.cellIndex);

        if (!model.isAvailableToSelect)
        {
            return;
        }

        OnControllerSelected?.Invoke(view.gameObject);
    }


    /// <summary>
    /// Mark that controller as available to click or not
    /// </summary>
    /// <param name="status"></param>
    public void MarkAsAvailableToSelected(bool status)
    {
        if (status)
        {
            model.isAvailableToSelect = true;
            view.HighlightField();
        }
        else
        {
            model.isAvailableToSelect = false;
            view.DisableField();
        }
        
    }

    /// <summary>
    /// Update field position on the game view
    /// </summary>
    /// <param name="targetPosition"></param>
    public void UpdateFieldPosition(Vector2 targetPosition)
    {
        view.SetPosition(targetPosition);
    }

    /// <summary>
    /// Change position in the grid model
    /// </summary>
    /// <param name="indexPosition"></param>
    public void AssingIndexPositionInArray(Vector2 indexPosition)
    {
        model.positionOnGrid = indexPosition;
    }
}
