﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleCellController : ICellAnimation
{
    public SingleCellModel model { get; private set; }

    public SingleCellView view { get; private set; }

    public SingleCellController(SingleCellModel _model, SingleCellView _view)
    {
        model = _model;
        view = _view;
    }

    /// <summary>
    /// Set the color for that cell
    /// </summary>
    /// <param name="color"></param>
    public void SetCellData(Color32 color)
    {
        view.ChangeSpriteColor(color);
        model.cellColor = color;
    }

    /// <summary>
    /// Trigger ScaleUp sprite
    /// </summary>
    public void TriggerScaleUpAnim()
    {
        view.ScaleUp();
    }

    /// <summary>
    /// Trigger ScaleDown sprite
    /// </summary>
    public void TriggerScaleDownAnim()
    {
        view.ScaleDown();
    }
}
