﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GridBoardController
{
    public static event Action OnCheckMatches;

    public GridBoardModel model { get; private set; }
    public GridBoardView view { get; private set; }

    public CellsProcessor cellsProcessor { get; private set; }

    List<GameObject> matchedFields = new List<GameObject>();

    /// <summary>
    /// Create new Board Controller
    /// </summary>
    /// <param name="_model"></param>
    /// <param name="_view"></param>
    /// <param name="_cellsProcessor"></param>
    public GridBoardController(GridBoardModel _model, GridBoardView _view, CellsProcessor _cellsProcessor)
    {
        model = _model;
        view = _view;
        cellsProcessor = _cellsProcessor;
    }

    /// <summary>
    /// Switch data in the grid model.
    /// Swap position of the 2 fields.
    /// </summary>
    /// <param name="originIndex"></param>
    /// <param name="targetIndex"></param>
    public void SwitchFieldOnGrid(Vector2 originIndex, Vector2 targetIndex)
    {
        var originField = model.boardFields[(int)originIndex.x, (int)originIndex.y];
        var targetField = model.boardFields[(int)targetIndex.x, (int)targetIndex.y];

        model.boardFields[(int)originIndex.x, (int)originIndex.y] = targetField;
        model.boardFields[(int)targetIndex.x, (int)targetIndex.y] = originField; ;
    }

    /// <summary>
    /// Find all matches on the grid based on data in grid model
    /// </summary>
    /// <returns></returns>
    public List<GameObject> FindMatches()
    {
        var matches = new List<GameObject>();
        matches.AddRange(FindMatchesInRows());
        matches.AddRange(FindInColumns());

        return matches;
    }


    /// <summary>
    /// Randomize data on start to avoid matches at the begging when initializing the board.
    /// </summary>
    /// <param name="seed"></param>
    public void RandomizeOnInit(int seed)
    {     
        #region rows
        //Find in rows
        for (int row = 0; row < model.rows; row++)
        {
            for (int column = 0; column < model.columns; column++)
            {
                //assign origin field
                var originField = model.boardFields[row, column];

                //assing init color
                var initColor = cellsProcessor.GetSingleController(originField.GetComponentInChildren<SingleCellView>()).model.cellColor;

                //Debug.Log("Origin Field: " + originField.name);
                //Debug.Log("Init Color: " + initColor);

                //Check every matches in the specific row
                for (int innerColumn = column + 1; innerColumn < model.columns; innerColumn++)
                {
                    var fieldToCheck = model.boardFields[row, innerColumn];
                    var cellController = cellsProcessor.GetSingleController(fieldToCheck.GetComponentInChildren<SingleCellView>());
                    var cellColor = cellController.model.cellColor;
                    List<Color32> colorsList = new List<Color32>(cellsProcessor.GetAvailableColors());

                    //Debug.Log("Nearest Cell Color: " + cellColor);

                    if (cellColor.Equals(initColor))
                    {
                        matchedFields.Add(fieldToCheck);

                        if (matchedFields.Count > 1)
                        {
                            colorsList.Remove(initColor);

                            UnityEngine.Random.InitState(seed);
                            cellController.SetCellData(colorsList[UnityEngine.Random.Range(0, colorsList.Count)]);
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                matchedFields.Clear();

                //Debug.Log("--------------------");
            }
        }
        #endregion

        #region columns
        //Find in columns
        for (int column = 0; column < model.columns; column++)
        {
            for (int row = 0; row < model.rows; row++)
            {
                //assign origin field
                var originField = model.boardFields[row, column];

                //assing init color
                var initColor = cellsProcessor.GetSingleController(originField.GetComponentInChildren<SingleCellView>()).model.cellColor;

                //Check every matches in the specific row
                for (int innerRows = row + 1; innerRows < model.rows; innerRows++)
                {
                    var fieldToCheck = model.boardFields[innerRows, column];
                    var cellController = cellsProcessor.GetSingleController(fieldToCheck.GetComponentInChildren<SingleCellView>());
                    var cellColor = cellController.model.cellColor;
                    List<Color32> colorsList = new List<Color32>(cellsProcessor.GetAvailableColors());

                    //Debug.Log("Nearest Cell Color: " + cellColor);

                    if (cellColor.Equals(initColor))
                    {
                        matchedFields.Add(fieldToCheck);

                        if (matchedFields.Count > 1)
                        {
                            colorsList.Remove(initColor);

                            UnityEngine.Random.InitState(seed);
                            cellController.SetCellData(colorsList[UnityEngine.Random.Range(0, colorsList.Count)]);
                        }
                    }
                    else
                    {
                        break;
                    }
                }

                matchedFields.Clear();

                //Debug.Log("--------------------");
            }
        }
        #endregion
    }


    /// <summary>
    /// Find and return matches in the rows
    /// </summary>
    /// <returns></returns>
    private List<GameObject> FindMatchesInRows()
    {
        List<GameObject> allMatches = new List<GameObject>();

        //Find in rows
        for (int row = 0; row < model.rows; row++)
        {
            for (int column = 0; column < model.columns; column++)
            {
                //assign origin field
                var originField = model.boardFields[row, column];

                //assing init color
                var initColor = cellsProcessor.GetSingleController(originField.GetComponentInChildren<SingleCellView>()).model.cellColor;

                //Check every matches in the specific row
                for (int innerColumn = column + 1; innerColumn < model.columns; innerColumn++)
                {
                    var fieldToCheck = model.boardFields[row, innerColumn];
                    if(fieldToCheck == null)
                    {
                        Debug.LogError("Field doestny exist !");
                    }

                    if(cellsProcessor.GetSingleController(fieldToCheck.GetComponentInChildren<SingleCellView>()) == null)
                    {
                        var fieldView = fieldToCheck.GetComponentInChildren<SingleCellView>();
                        var cellController = cellsProcessor.GetSingleController(fieldView);
                        Debug.LogError("Controller doesnt exist !");
                    }

                    var cellColor = cellsProcessor.GetSingleController(fieldToCheck.GetComponentInChildren<SingleCellView>()).model.cellColor;

                    //Debug.Log("Nearest Cell Color: " + cellColor);

                    if (cellColor.Equals(initColor))
                    {
                        matchedFields.Add(fieldToCheck);
                    }
                    else
                    {
                        break;
                    }

                    //Debug.Log("Found: " + fieldToCheck.name);
                }

                if (matchedFields.Count > 1)
                {
                    
                    foreach(var match in matchedFields)
                    {
                        if (!allMatches.Contains(match))
                        {
                            allMatches.Add(match);
                        }
                    }

                    if (!allMatches.Contains(originField))
                    {
                        allMatches.Add(originField);
                    }

                }

                matchedFields.Clear();
            }
        }

        return allMatches;
    }

    /// <summary>
    /// Find and return matches in the column
    /// </summary>
    /// <returns></returns>
    private List<GameObject> FindInColumns()
    {
        List<GameObject> allMatches = new List<GameObject>();

        //Find in columns
        for (int column = 0; column < model.columns; column++)
        {
            for (int row = 0; row < model.rows; row++)
            {
                //assign origin field
                var originField = model.boardFields[row, column];

                //assing init color
                var initColor = cellsProcessor.GetSingleController(originField.GetComponentInChildren<SingleCellView>()).model.cellColor;

                Debug.Log("Origin Field: " + originField.name);
                Debug.Log("Init Color: " + initColor);

                //Check every matches in the specific row
                for (int innerRows = row + 1; innerRows < model.rows; innerRows++)
                {
                    var fieldToCheck = model.boardFields[innerRows, column];
                    var cellColor = cellsProcessor.GetSingleController(fieldToCheck.GetComponentInChildren<SingleCellView>()).model.cellColor;

                    Debug.Log("Nearest Cell Color: " + cellColor);

                    if (cellColor.Equals(initColor))
                    {
                        matchedFields.Add(fieldToCheck);
                    }
                    else
                    {
                        break;
                    }

                    Debug.Log("Found: " + fieldToCheck.name);
                }

                if (matchedFields.Count > 1)
                {
                    Debug.Log("Found match in: " + originField.name);

                    foreach (var match in matchedFields)
                    {
                        if (!allMatches.Contains(match))
                        {
                            allMatches.Add(match);
                        }
                    }

                    if (!allMatches.Contains(originField))
                    {
                        allMatches.Add(originField);
                    }
                }

                matchedFields.Clear();
            }
        }

        return allMatches;
    }
}
