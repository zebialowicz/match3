﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GridLayoutGroup))]
[RequireComponent(typeof(RectTransform))]
[ExecuteInEditMode]
public class GridDesigner : MonoBehaviour
{
    #region UI
    [Header("UI Properties")]
    public GridLayoutGroup UI_GridLayout;
    public RectTransform Container;
    #endregion

    #region Grid Editor Properties
    [Header("Grid Properties")]
    [Range(3,9)]
    public int gridRows = 3;

    [Range(3, 9)]
    public int gridColumns = 3;

    [Range(0, 1024)]
    public int seed = 0;

    private int _localGridRows = 3;
    private int _localGridColumns = 3;

    public Color32[] AvailableColors = new Color32[6];
    #endregion


    [SerializeField]
    private GameObject gridFieldPrefab;

    [SerializeField]
    private List<GameObject> currentFields = new List<GameObject>();

    /// <summary>
    /// Update grid view out of runtime if change any value
    /// </summary>
    private void Update()
    {
        if (Application.isPlaying)
        {
            return;
        }

        if(gridRows == _localGridRows && gridColumns == _localGridColumns)
        {
            return;
        }

        _localGridRows = gridRows;
        _localGridColumns = gridColumns;

        clearFields();
        SpawnFields();
        updateContainerSize();
    }

    /// <summary>
    /// Destroy field if global amount is bigger then size in designer
    /// </summary>
    void clearFields()
    {
        foreach (var field in currentFields)
        {
            DestroyImmediate(field);
        }

        currentFields.Clear();
    }


    /// <summary>
    /// Spawn new fields to match width and height of the container
    /// </summary>
    void SpawnFields()
    {
        if (currentFields.Count >= gridColumns * gridRows)
        {
            return;
        }

        for (int columns = 0; columns < gridColumns; columns++)
        {
            for(int rows = 0; rows < gridRows; rows++)
            {
                var field = Instantiate(gridFieldPrefab, UI_GridLayout.transform);
                field.name = "[" + columns.ToString() + rows.ToString() + "]";
                currentFields.Add(field);
            }
        }
    }

    /// <summary>
    /// Change size of container for fields
    /// </summary>
    void updateContainerSize()
    {
        if(Container == null)
        {
            Container = GetComponent<RectTransform>();
        }

        Container.sizeDelta = new Vector2(gridColumns * 100, gridRows * 100);
    }


    /// <summary>
    /// Get List of Fields created out of runtime on the Grid
    /// </summary>
    /// <returns></returns>
    public List<GameObject> GetGridFields()
    {
        return currentFields;
    }

}
